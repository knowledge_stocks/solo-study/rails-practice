class HomeController < ApplicationController
	# index 메소드 추가
	# Rails는 자유도가 높지는 않다. 기본적으로 정해진 규칙을 따르는 것으로 생산성을 높일 수 있다.
	# views/home/index.erb에 자동으로 매칭된다.
	def index
	end
	
	def menu
		# 인스턴스 변수를 이런 경우에 사용하는가보다.
		@menuItems = ['Home', 'Intro']
	end
end
